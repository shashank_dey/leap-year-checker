from unit_testing.LeapYearCheckerTestClass import TestForLeapYear


# Get "False" when passed 1
def test_get_false_with_passed_1():
    obj = TestForLeapYear()
    obj.check(1, False)


# Get "False" when passed 2
def test_get_false_with_passed_2():
    obj = TestForLeapYear()
    obj.check(2, False)


# Get "False" when passed 3
def test_get_false_with_passed_3():
    obj = TestForLeapYear()
    obj.check(3, False)


# Get "True" when passed 400
def test_get_true_with_passed_400():
    obj = TestForLeapYear()
    obj.check(400, True)


# Get "True" when passed 800 (multiple of 400)
def test_get_true_with_passed_800():
    obj = TestForLeapYear()
    obj.check(800, True)


# Get "True" when passed 804 (multiple of 4)
def test_get_true_with_passed_804():
    obj = TestForLeapYear()
    obj.check(804, True)


# Get "False" when passed 500 (multiple of 100 but not 400)
def test_get_false_with_passed_500():
    obj = TestForLeapYear()
    obj.check(500, False)


# Get "False" when passed 2022 (not a multiple of 4)
def test_get_false_with_passed_2022():
    obj = TestForLeapYear()
    obj.check(2022, False)


# Get "True" when passed 2024 (multiple of 4 but not 100)
def test_get_true_with_passed_2024():
    obj = TestForLeapYear()
    obj.check(2024, True)
